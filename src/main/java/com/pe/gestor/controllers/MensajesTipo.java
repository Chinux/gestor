package com.pe.gestor.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pe.gestor.service.LoggerService;	

@Controller
public class MensajesTipo {

	@Autowired
	LoggerService loggerService;
	
	
	@RequestMapping(value = "/api/registrarLog", method = RequestMethod.POST)
	public  String registrarTipoMensaje(@RequestBody String[] TiposMensaje
			) {
		   
		
		loggerService.registrarLogger(TiposMensaje);
		
		return "home";
		
	}
}
