package com.pe.gestor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pe.gestor.logger.JobLogger;

@SpringBootApplication
public class GestorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestorApplication.class, args);
		
		boolean logToFileParam = true;
		boolean logToConsoleParam = true;
		boolean logToDatabaseParam = true;
		boolean logMessageParam = true;
		boolean logWarningParam = true;
		boolean logErrorParam = true;
		Map<String, String> dbParamsMap = new HashMap<String,String>();
		dbParamsMap.put("userName", "root");
		dbParamsMap.put("password", "slackwarex");
		dbParamsMap.put("serverName", "localhost");
		dbParamsMap.put("dbms", "mysql");
		dbParamsMap.put("logFileFolder", "src/main/resources/Logs/");
		dbParamsMap.put("portNumber", "3306");
		
		 
		new JobLogger(logToFileParam, logToConsoleParam, logToDatabaseParam, logMessageParam, logWarningParam, logErrorParam, dbParamsMap);
		
	 

	}
}
