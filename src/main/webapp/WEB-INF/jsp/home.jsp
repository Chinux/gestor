<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
  <head>  
  
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">  
 
   <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
   <script
	src="${pageContext.request.contextPath}/js/main.js"></script>
  </head>
  <body>



    <div class="container">
     
   <div class="panel panel-primary">
      <div class="panel-heading">Pruebas de Registro de Log</div>
      <div class="panel-body"><form action="/" > 
 

  <div class="checkbox">
    <label>
      <input type="checkbox" name="Logger[]" value="Message"> Message
    </label>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="Logger[]" value="Error">  Error
    </label>
  </div>
  <div class="checkbox">
    <label>
      <input type="checkbox" name="Logger[]" value="Warning">  Warning
    </label>
  </div>
  
  
  
  
  <div id="respuesta"></div>
</form></div>
         <div class = "panel-footer"><button type="button" class="btn btn-default" onclick="TipoMensaje()">Generar Error</button></div>

    </div>

    </div> <!-- /container -->


  </body>

</html>